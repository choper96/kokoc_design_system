module.exports = {
  'files': [
    './ico_font/*.svg'
  ],
  'fontName': 'ico_font',
  'classPrefix': 'icon-',
  'baseSelector': '.ico_font',
  'types': ['eot', 'woff', 'woff2', 'ttf', 'svg'],
  'fileName': 'app.[ico_font].[hash].[ext]'
};